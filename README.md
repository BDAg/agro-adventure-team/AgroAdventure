### Esse repositório é destinado aos arquivos que compoem o jogo, caso queira ver nossas documentações e nosso time de desenvolvimento, [clique aqui](https://gitlab.com/BDAg/agro-adventure-team/repositorio-de-documentos)

### Para inicalizar esse diretório em sua máquina, já com o Unreal instalado os passos são:
1. Navegar até Documentos/Unreal Projects ( Caso não possua, crie o diretório )
2. Clonar esse repositório
3. Acessar o link [ hospedados aqui](https://drive.google.com/drive/folders/1BKr4shPLmH9O-pRs-rc6UAsuS_2Qvqt_?usp=sharing), baixar a pasta `Arquivos que não estão no git`
4. Colocar os arquivos que estão dentro de `Arquivos que não estão no git` na raiz da pasta AgroAdventure

### Os arquivos faltantes estão [ hospedados aqui](https://drive.google.com/drive/folders/1BKr4shPLmH9O-pRs-rc6UAsuS_2Qvqt_?usp=sharing) por serem considerávelmente pesados


### Download Unreal:
[Link para download](https://www.unrealengine.com/en-US/download)