// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/Online/HTTP/Public/Http.h"
#include "Json.h"
#include "JsonUtilities.h"
#include "HTTPService.generated.h"

USTRUCT()
struct FRequest_Feedback {
	GENERATED_BODY()
	UPROPERTY() int mission_id;
	UPROPERTY() FString message;

	FRequest_Feedback() {}
};

USTRUCT()
struct FCallback_Struct {
	GENERATED_BODY()
	UPROPERTY() FString _id;
	UPROPERTY() FString message;
};

USTRUCT()
struct FResponse_Feedback {
	GENERATED_BODY()
	UPROPERTY() FCallback_Struct callback;
	UPROPERTY() FString message;

	FResponse_Feedback() {}
};

UCLASS()
class AGROADVENTURE_API AHTTPService : public AActor
{
	GENERATED_BODY()


private:	
	FHttpModule* Http;
	FString ApiBaseUrl = "http://localhost:3333";

	TSharedRef<IHttpRequest> RequestWithRoute(FString Subroute);
	void SetRequestHeaders(TSharedRef<IHttpRequest>& Request);

	TSharedRef<IHttpRequest> GetRequest(FString Subroute);
	TSharedRef<IHttpRequest> PostRequest(FString Subroute, FString ContentJsonString);
	void Send(TSharedRef<IHttpRequest>& Request);

	bool ResponseIsValid(FHttpResponsePtr Response, bool bWasSuccessful);

	template <typename StructType>
	void GetJsonStringFromStruct(StructType FilledStruct, FString& StringOutput);
	template <typename StructType>
	void GetStructFromJsonString(FHttpResponsePtr Response, StructType& StructOutput);

public:	
	// Called every frame
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = "Functions")
		void SendRequest(FString message);

	AHTTPService();

	void Feedback(FRequest_Feedback LoginCredentials);
	void FeedbackResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
};
