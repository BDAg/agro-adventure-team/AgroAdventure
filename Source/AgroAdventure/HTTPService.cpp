// Fill out your copyright notice in the Description page of Project Settings.

#include "AgroAdventure.h"
#include "HTTPService.h"

// Sets default values
AHTTPService::AHTTPService()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AHTTPService::BeginPlay()
{
	Super::BeginPlay();
	Http = &FHttpModule::Get();
}

void AHTTPService::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AHTTPService::Send(TSharedRef<IHttpRequest>& Request) {
	Request->ProcessRequest();
}

bool AHTTPService::ResponseIsValid(FHttpResponsePtr Response, bool bWasSuccessful) {
	if (!bWasSuccessful || !Response.IsValid()) return false;
	if (EHttpResponseCodes::IsOk(Response->GetResponseCode())) return true;
	else {
		UE_LOG(LogTemp, Warning, TEXT("Http Response returned error code: %d"), Response->GetResponseCode());
		return false;
	}
}

template <typename StructType>
void AHTTPService::GetJsonStringFromStruct(StructType FilledStruct, FString& StringOutput) {
	FJsonObjectConverter::UStructToJsonObjectString(StructType::StaticStruct(), &FilledStruct, StringOutput, 0, 0);
}

template <typename StructType>
void AHTTPService::GetStructFromJsonString(FHttpResponsePtr Response, StructType& StructOutput) {
	StructType StructData;
	FString JsonString = Response->GetContentAsString();
	FJsonObjectConverter::JsonObjectStringToUStruct<StructType>(JsonString, &StructOutput, 0, 0);
}

TSharedRef<IHttpRequest> AHTTPService::RequestWithRoute(FString Subroute) {
	TSharedRef<IHttpRequest> Request = Http->CreateRequest();
	Request->SetURL(ApiBaseUrl + Subroute);
	SetRequestHeaders(Request);
	return Request;
}

void AHTTPService::SetRequestHeaders(TSharedRef<IHttpRequest>& Request) {
	Request->SetHeader(TEXT("User-Agent"), TEXT("X-UnrealEngine-Agent"));
	Request->SetHeader(TEXT("Content-Type"), TEXT("application/json"));
	Request->SetHeader(TEXT("Accepts"), TEXT("application/json"));
}

TSharedRef<IHttpRequest> AHTTPService::GetRequest(FString Subroute) {
	TSharedRef<IHttpRequest> Request = RequestWithRoute(Subroute);
	Request->SetVerb("GET");
	return Request;
}

TSharedRef<IHttpRequest> AHTTPService::PostRequest(FString Subroute, FString ContentJsonString) {
	TSharedRef<IHttpRequest> Request = RequestWithRoute(Subroute);
	Request->SetVerb("POST");
	Request->SetContentAsString(ContentJsonString);
	return Request;
}

void AHTTPService::Feedback(FRequest_Feedback FeedbackCredentials) {
	FString ContentJsonString;
	GetJsonStringFromStruct<FRequest_Feedback>(FeedbackCredentials, ContentJsonString);

	TSharedRef<IHttpRequest> Request = PostRequest("/feedback", ContentJsonString);
	Request->OnProcessRequestComplete().BindUObject(this, &AHTTPService::FeedbackResponse);
	Send(Request);
}

void AHTTPService::FeedbackResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful) {
	if (!ResponseIsValid(Response, bWasSuccessful)) return;

	FResponse_Feedback FeedbackResponse;
	GetStructFromJsonString<FResponse_Feedback>(Response, FeedbackResponse);

	UE_LOG(LogTemp, Warning, TEXT("Id is: %s"), *FeedbackResponse.callback._id);
	UE_LOG(LogTemp, Warning, TEXT("Response is: %s"), *FeedbackResponse.message);
	UE_LOG(LogTemp, Warning, TEXT("Message is: %s"), *FeedbackResponse.callback.message);
}

void AHTTPService::SendRequest(FString message) {
	FRequest_Feedback FeedbackCredentials;
	FeedbackCredentials.mission_id = 1;
	FeedbackCredentials.message = message;
	Feedback(FeedbackCredentials);
}